const assert = require('assert');
const math = require('./math');

describe('file to be tested',()=>{
    context('function to be tested',()=>{
        it('should do something',()=>{
            assert.equal(1,1)
        })
        it('should do another thing',()=>{
            assert.deepEqual([1,2,3], [1,2,3])
            assert.deepEqual({name : 'a'},{name:'a'})
        })
    })
})

describe('file math',()=>{
    context('function add1', ()=>{
        it('Should add1(1,2)',()=>{
            assert.equal(math.add1(1,2), 3)
        })
    })
    context('function add2',()=>{
        it('Should add2(5,5)',()=>{
            assert.equal(math.add2(5,5),10)
        })
    })
})