const chai  = require('chai');
const expect = chai.expect;
const math = require('./math')

describe('Test chai',()=>{
    it('should compare thing by expect',()=>{
        expect(1).to.equal(1)
    })
    it('should compare another thing by expect',()=>{
        expect(5>8).to.be.false;
        expect({name:'a'}).to.deep.equal({name:'a'})
        expect({name:'a'}).to.have.property('name').to.equal('a')
        expect({}).to.be.a('object')
        expect(1).to.be.a('number')
        expect('a').to.be.a('string')
        expect('aaa'.length).to.equal(3)
        expect('aaa').to.lengthOf(3)
        expect([1,2,3]).to.lengthOf(3)
        expect(null).to.be.null
        expect(undefined).to.not.exist
        expect(1).to.exist
    })
})

describe('Math Module',()=>{
    context('Function add1',()=>{
        it('ควรส่งค่ากลับเป็นตัวเลข',()=>{
            expect(math.add1(0,0)).to.be.a('number')
         })
        it('add(1,1)ควนส่งค่ากลับเป็น 2',()=>{
            expect(math.add1(1,1)).to.equal(2)

        })
    })
})